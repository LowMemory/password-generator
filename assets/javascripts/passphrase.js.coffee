#-------------------------------------------------------------------
# Copyright (c) 2011 Jeff Preshing
# Copyright 2012 Jean-Philippe Garcia Ballester <jeanphilippe@ac-grenoble.fr>, SLIS Team, CARMI-Internet
# http://passphra.se/
# All rights reserved.
#
# Some parts based on http://www.mytsoftware.com/dailyproject/PassGen/entropy.js, copyright 2003 David Finch.
# 
# Released under the Modified BSD License:
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#-------------------------------------------------------------------

#= require sha1-min

# Get some entropy from the system clock:
passphrase_time_ent = () ->
  d = 1 * new Date
  i = 0
  while 1 * new Date == d
    i++ # Measure iterations until next tick
  "" + d + i

# Return a pseudorandom array of four 32-bit integers:
passphrase_create_hash = () ->
  # Entropy string built in a manner inspired by David Finch:
  # passphrase_server_hash should be defined in the file including this script
  # with e.g. <script>var passphrase_server_hash = "<%= SecureRandom.hex %>";</script>
  entropy = passphrase_server_hash + passphrase_time_ent
  entropy += navigator.userAgent + Math.random + Math.random + screen.width + screen.height
  if document.all
    entropy = entropy + document.body.clientWidth + document.body.clientHeight + document.body.scrollWidth + document.body.scrollHeight
  else
    entropy = entropy + window.innerWidth + window.innerHeight + window.width + window.height
  entropy += passphrase_time_ent
      
  # Hash and convert to 32-bit integers:
  hexString = hex_sha1 entropy # from sha1-min.js
  (parseInt (hexString.substr i, 8), 16 for i in [0..31] by 8)

# Given a hash and a word number, returns a word from the wordlist
choice = (hash, w) ->
  jsRandom = Math.floor(Math.random() * 0x100000000)
  index = ((jsRandom ^ hash[w]) + 0x100000000) % passphrase_wordlist.length
  passphrase_wordlist[index]

# Generate a new passphrase and update the document:
window.passphrase_gen = () ->
    hash = passphrase_create_hash()
    choices = (choice hash, w for w in [0..3])
    resultSpan = document.getElementById "passphrase_result"
    resultSpan.innerText = resultSpan.textContent = choices.join " "
    shortResultSpan = document.getElementById "passphrase_short_result"
    shortResultSpan.innerText = shortResultSpan.textContent = choices.join ""

passphrase_wordlist = [
  "abandonne", "abandonner", "abattre", "abeille", "abime", "abricot", "absence",
  "absent", "absolu", "absolument", "accent", "accepter", "accident",
  "accompagner", "accomplir", "accord", "accorder", "accoucher", "accrocher",
  "accuser", "acheter", "achever", "acrobate", "acteur", "action", "activites",
  "actuel", "actuellement", "admettre", "administration", "adorer", "adresse",
  "adresser", "adroit", "adulte", "aeroport", "affaire", "affiche", "affirmer",
  "agacer", "agent", "agiter", "agneau", "aider", "aigle", "aiguille", "ailleurs",
  "aimer", "ainsi", "ajouter", "album", "aliment", "aller", "allumer",
  "allumette", "alors", "alphabet", "ambulance", "amener", "amour", "ampoule",
  "amusant", "amuser", "ananas", "ancienne", "anglais", "angle", "angoisse",
  "animal", "animaux", "animer", "annexes", "anniversaire", "annonce", "annoncer",
  "anorak", "apercevoir", "apparait", "apparaitre", "appareil", "apparence",
  "appartement", "appartenir", "appeler", "appetit", "apporter", "apprendre",
  "approcher", "appuyer", "apres", "aquarium", "araignee", "arbre", "arete",
  "argent", "armee", "armer", "armoire", "arracher", "arranger", "arret",
  "arreter", "arriere", "arriver", "arroser", "arrosoir", "artistes", "ascenseur",
  "aspect", "aspirateur", "asseoir", "assez", "assiette", "assis", "assister",
  "assurer", "attacher", "attaquer", "atteindre", "atteint", "attendre",
  "attention", "atterrir", "attirer", "attitude", "attraper", "aupres", "auquel",
  "aurait", "aussi", "aussitot", "autant", "auteur", "autorite", "autour",
  "autrefois", "autrement", "avaient", "avait", "avaler", "avance", "avancer",
  "avant", "avenir", "aventure", "avion", "avoir", "avouer", "avril", "ayant",
  "bagage", "bagarre", "bague", "baguette", "baigner", "bailler", "baiser",
  "baisser", "balai", "balancoire", "balayer", "balcon", "baleine", "balle",
  "ballon", "banane", "bande", "barbe", "barboter", "barbouille", "barque",
  "barre", "barreau", "barrer", "barrette", "bassin", "bassine", "bataille",
  "bateau", "baton", "battre", "baver", "bavoir", "beaucoup", "beaute", "beaux",
  "bebes", "belgique", "belle", "bercer", "besoin", "betes", "betise", "beurre",
  "biberon", "bibliographie", "bicyclette", "bientot", "bifteck", "bijou",
  "bille", "billet", "biographie", "biscuit", "bisou", "bizarre", "blanc",
  "blond", "boire", "boisson", "boite", "bonbon", "bondir", "bonheur", "bonhomme",
  "bonjour", "bonne", "bonnet", "bosse", "bosser", "bouche", "boucher",
  "boucherie", "bouchon", "bouder", "bouee", "bouger", "bouillir", "boulanger",
  "boulangerie", "boule", "boulot", "bouquet", "bourgeon", "bousculer",
  "bouteille", "boutique", "bouton", "bracelet", "branche", "bretelle",
  "bricolage", "briller", "briser", "britannique", "brosse", "brouette",
  "brouillard", "bruit", "bruler", "brusquement", "buche", "buisson", "bulles",
  "bureau", "cabane", "cabinet", "cache", "cacher", "cadeau", "cadenas", "cadre",
  "cagoule", "caillou", "caisse", "calendrier", "caliner", "calme", "calmer",
  "camarade", "camescope", "camion", "campagne", "camper", "canada", "canape",
  "canard", "caniveau", "canne", "canton", "capable", "capitaine", "capitale",
  "caprice", "caractere", "caravane", "caresse", "caresser", "carotte", "carreau",
  "carrefour", "carriere", "carte", "carton", "casque", "casquette", "casse",
  "casser", "casserole", "cassette", "categorie", "cauchemar", "cause", "causer",
  "ceder", "cederom", "ceinture", "celebre", "celui", "centrale", "centre",
  "cependant", "cerceau", "cercle", "cereale", "cerise", "certainement",
  "certaines", "certes", "cerveau", "cesse", "cesser", "cette", "chacun",
  "chaine", "chair", "chaleur", "chambre", "champ", "champignon", "champion",
  "championnat", "chance", "changea", "changement", "changer", "chant", "chanter",
  "chapeau", "chapelle", "chaque", "charcuterie", "charge", "charger", "chariot",
  "charles", "chasser", "chasseur", "chataigne", "chateau", "chaud", "chauffer",
  "chaussette", "chausson", "chaussure", "chemin", "cheminee", "chemise", "chene",
  "chenille", "chercher", "cheval", "cheveu", "cheville", "chevre", "chien",
  "chiffon", "chiffre", "chocolat", "choisir", "choix", "chose", "chouette",
  "chuchoter", "chute", "cigarette", "cigogne", "cimetiere", "cinema",
  "cinquante", "circonstance", "cirque", "citron", "citrouille", "classe",
  "classement", "classique", "clementine", "client", "clignoter", "cloche",
  "clocher", "clown", "clubs", "coccinelle", "cochon", "cocotte", "coeur",
  "coffre", "coffret", "coiffeur", "colere", "collaboration", "collant", "colle",
  "collection", "coller", "collier", "colline", "colon", "colonel", "colorier",
  "combat", "combien", "commander", "comme", "commence", "commencement",
  "commencer", "comment", "commerce", "communaute", "compagnie", "compagnon",
  "comparer", "competition", "completement", "composer", "comprend", "comprendre",
  "comptait", "compte", "compter", "comte", "concernant", "conclure", "concombre",
  "condamner", "conduire", "confiance", "confier", "confiture", "confondre",
  "confortable", "connaissance", "connaitre", "connexes", "conscience", "conseil",
  "consentir", "considere", "considerer", "consoler", "consomme", "constitue",
  "construction", "construire", "consulte", "consulter", "contenir", "contenter",
  "contient", "continue", "continuer", "contraire", "contrat", "contre",
  "controle", "convenir", "conversation", "copain", "copier", "coquelicot",
  "coquet", "coquetier", "coquillage", "coquille", "coquin", "corbeau",
  "corbeille", "corde", "corps", "cotes", "couche", "coucher", "coude", "coudre",
  "couette", "couler", "couloir", "coupe", "couper", "courage", "courant",
  "courir", "couronne", "courrier", "cours", "course", "court", "coussin",
  "couteau", "couter", "couvercle", "couverture", "couvrir", "crabe", "cracher",
  "craindre", "crainte", "crapaud", "cravate", "creation", "creche", "creee",
  "creer", "crepes", "creuser", "creux", "crevette", "crier", "crise", "critique",
  "crochet", "crocodile", "croire", "croiser", "croix", "croquer", "croute",
  "cruel", "cueillir", "cuillere", "cuire", "cuisine", "cuisiner", "cuisiniere",
  "cuisse", "culotte", "culture", "curieux", "curiosite", "cuvette", "cygne",
  "dangereux", "danse", "danser", "dauphin", "davantage", "david", "deborder",
  "debout", "decede", "decembre", "dechirer", "decide", "decider", "declarer",
  "decoller", "decorer", "decouper", "decouverte", "decouvrir", "decrire",
  "dedans", "defaut", "defendre", "defense", "defiler", "degager", "deguisement",
  "deguiser", "dehors", "dejeuner", "delicieux", "demain", "demande", "demander",
  "demarrer", "demenager", "demeurer", "demographie", "demolir", "dentifrice",
  "dentiste", "departement", "depasser", "deposer", "depuis", "depute",
  "deranger", "derriere", "descendre", "description", "desert", "desespoir",
  "designe", "designer", "desirer", "desobeir", "desole", "desoler", "desordre",
  "desormais", "dessert", "dessiner", "dessus", "detacher", "detail", "detester",
  "detruire", "deuxieme", "devait", "devant", "developpement", "devenir",
  "devenu", "devient", "deviner", "devint", "devoir", "difference", "differentes",
  "difficile", "digne", "dimanche", "dindon", "diner", "dinette", "dinosaure",
  "directement", "directeur", "direction", "directrice", "diriger", "discours",
  "discussion", "discuter", "disparaitre", "dispose", "disposer", "distance",
  "distinguer", "distribuer", "distribution", "district", "diverses", "division",
  "docteur", "doivent", "dollar", "domaine", "dominer", "donnees", "donner",
  "dormir", "dossier", "double", "doucement", "douceur", "douche", "doucher",
  "douillet", "douleur", "doute", "douter", "douze", "drame", "drapeau",
  "dresser", "droite", "drole", "durant", "durer", "ecarter", "echanger",
  "echapper", "echarpe", "echelle", "eclabousser", "eclairer", "eclater", "ecole",
  "economique", "ecorce", "ecouter", "ecran", "ecraser", "ecrire", "ecrit",
  "ecriture", "ecureuil", "effacer", "effet", "effort", "effraye", "egalement",
  "eglise", "elastique", "elections", "electrique", "elephant", "eleve", "elever",
  "elle", "eloigner", "embouteillage", "embrasser", "emmener", "emotion",
  "empecher", "empire", "employer", "emporter", "enceinte", "encore", "endive",
  "endormir", "endroit", "energie", "enerve", "enfance", "enfermer", "enfiler",
  "enfin", "enfoncer", "engager", "engin", "enlever", "ennemi", "enorme",
  "ensemble", "ensuite", "entendre", "enterrer", "entier", "entonnoir",
  "entourer", "entrainer", "entreprises", "entrer", "entretenir", "enveloppe",
  "envelopper", "envie", "environ", "envoyer", "epais", "epaule", "epingle",
  "eplucher", "epluchure", "eponge", "epoque", "epouse", "eprouver", "erreur",
  "escabeau", "escalader", "escalier", "escargot", "espace", "esperer", "espoir",
  "esprit", "essayer", "essence", "essentiellement", "essuyer", "etablir",
  "etage", "etagere", "etaient", "etait", "etaler", "etang", "etant", "etape",
  "eteindre", "etendre", "etendue", "eternel", "eternuer", "etoile", "etonner",
  "etouffer", "etrange", "etranger", "etroit", "etudier", "europe", "evenement",
  "evier", "eviter", "examiner", "excuser", "executer", "exemple", "exiger",
  "existe", "existence", "exister", "experience", "expliquer", "exposer",
  "expression", "exprimer", "exterieur", "extraordinaire", "fabriquer", "facile",
  "facon", "facteur", "faible", "faire", "faisant", "falloir", "famille", "farce",
  "farine", "fatigue", "fatiguer", "faute", "fauteuil", "faveur", "fenetre",
  "ferme", "fermer", "fermier", "fesse", "feuille", "fevrier", "ficelle", "fiche",
  "fidele", "fievre", "figure", "figurer", "filer", "filet", "fille", "films",
  "finale", "finalement", "finir", "fixer", "flamme", "flaque", "fleche", "fleur",
  "fleuriste", "flocon", "flotter", "foire", "folie", "fonce", "fonder",
  "fontaine", "football", "forcer", "foret", "formation", "former", "forte",
  "fortune", "fouiller", "foule", "fourchette", "fourmi", "foutre", "frais",
  "fraise", "framboise", "france", "franchir", "francois", "frange", "frapper",
  "frein", "frere", "frite", "froid", "fromage", "front", "frotter", "fruit",
  "fumee", "fumer", "furent", "fusee", "fusil", "gagner", "galerie", "galette",
  "galoper", "garage", "garcon", "garde", "garder", "gardien", "garer", "gateau",
  "gauche", "geant", "geler", "generalement", "genou", "genre", "gentil",
  "geographie", "geographique", "geste", "gestion", "girafe", "glace", "glacon",
  "glisser", "gloire", "gobelet", "gonfler", "gorge", "gosse", "gourde",
  "gourmand", "gouter", "gouvernement", "grace", "grandir", "gratter", "grave",
  "grenouille", "griffe", "griffer", "grignoter", "griller", "grimace", "grimper",
  "gronder", "grotte", "guepe", "guere", "gueri", "guerir", "guerre", "guetter",
  "gueule", "guirlande", "gymnastique", "habiller", "habiter", "habitude",
  "haine", "hamster", "hanche", "handicape", "haricot", "hasard", "haute",
  "hauteur", "helicoptere", "herbe", "herisson", "hesiter", "heureux", "hibou",
  "hippopotame", "hirondelle", "histoire", "historique", "hiver", "honneur",
  "honte", "hopital", "horizon", "horloge", "hotel", "huile", "humain", "humeur",
  "humide", "hurler", "ignorer", "image", "imaginer", "imiter", "immense",
  "immeuble", "immobile", "importance", "importer", "imposer", "impossible",
  "impression", "incendie", "inconnu", "indiquer", "inonder", "inquieter",
  "inquietude", "insecte", "inseparable", "inspirer", "instable", "installer",
  "instant", "instinct", "instrument", "intelligence", "intention", "interessant",
  "interesser", "interet", "interieur", "internes", "interroger", "interrompre",
  "intrus", "inutile", "inventer", "invitation", "inviter", "jacques", "jaloux",
  "jamais", "jambon", "janvier", "japon", "jardiner", "jaune", "jeter", "jeudi",
  "jeunesse", "joindre", "jongler", "jonquille", "jouer", "journal", "journaux",
  "journee", "jours", "joyeux", "juger", "juillet", "jumeau", "jumelles", "jurer",
  "jusque", "juste", "justice", "kangourou", "kilometres", "lacer", "lacet",
  "lacher", "laine", "laisse", "laisser", "lampe", "lancer", "langue", "lapin",
  "laquelle", "large", "larme", "lavabo", "laver", "lecher", "lecon", "lecture",
  "leger", "legume", "lendemain", "lentement", "lequel", "lessive", "lever",
  "lezard", "liberte", "libre", "liees", "liens", "lieux", "linge", "liquide",
  "lisse", "lisser", "liste", "litre", "livrer", "londres", "longtemps", "longue",
  "longueur", "lorsque", "louche", "louis", "loupe", "lourd", "lueur", "lumiere",
  "lundi", "lunettes", "lutin", "lutte", "lutter", "machine", "madame",
  "mademoiselle", "magasin", "magicien", "magie", "magnetoscope", "magnifique",
  "maigre", "maillot", "mains", "maintenant", "maintenir", "maire", "maison",
  "maitresse", "majorite", "malade", "maladie", "maladroit", "malgre",
  "malheureux", "maman", "manche", "manege", "manger", "manier", "maniere",
  "manquer", "manteau", "maquillage", "maquiller", "marchand", "marche",
  "marcher", "mardi", "marguerite", "mariage", "marie", "marier", "marin",
  "marionnette", "marque", "marquer", "marron", "marteau", "masque", "masse",
  "matelas", "maternelle", "matiere", "matin", "mauvais", "mechant", "medaille",
  "medecin", "medicament", "melanger", "meler", "melon", "memoire", "menacer",
  "menage", "mener", "mensonge", "mentir", "menton", "mercredi", "merde",
  "meriter", "mesure", "mesurer", "metal", "mettre", "meuble", "meurt", "michel",
  "micro", "mieux", "milieu", "mille", "mince", "ministre", "minuit", "miser",
  "mission", "mixer", "modele", "moderne", "moindre", "moineau", "moins",
  "moitie", "moment", "monde", "mondiale", "monnaie", "monsieur", "montagne",
  "monter", "montre", "montrer", "morceau", "mordre", "moteur", "mouche",
  "mouchoir", "mouette", "moufle", "mouille", "mouiller", "moule", "moulin",
  "mourir", "mousse", "moustache", "moustique", "mouton", "mouvement", "moyen",
  "moyenne", "muguet", "multicolore", "municipalite", "muscle", "musee",
  "musique", "nager", "naissance", "naitre", "nappe", "nature", "naturel",
  "naturellement", "navet", "navire", "necessaire", "neige", "neiger", "nerveux",
  "nettoyer", "niveau", "noire", "noisette", "nombre", "nombreuses", "nombreux",
  "nomme", "nommer", "notamment", "notes", "notre", "nourrir", "nourriture",
  "nouveau", "nouveaux", "novembre", "noyau", "nuage", "nuageux", "numero",
  "obeir", "objet", "obliger", "observer", "obtenir", "obtient", "occasion",
  "occuper", "octobre", "odeur", "oeuvre", "officiel", "officier", "offrir",
  "oignon", "oiseau", "ombre", "oncle", "ongle", "orage", "orange", "orchestre",
  "ordinateur", "ordonnance", "ordre", "oreille", "oreiller", "oublier",
  "ouragan", "outil", "outre", "ouvrage", "ouvrier", "ouvrir", "pages", "paille",
  "paire", "palais", "palmares", "pamplemousse", "panda", "panier", "panne",
  "panneau", "pansement", "pantalon", "panthere", "papier", "papillon",
  "paquerette", "paquet", "paraitre", "parapluie", "parasol", "parce",
  "parcourir", "parcours", "pardon", "pareil", "parent", "parfaitement",
  "parfois", "parfum", "paris", "parking", "parler", "parmi", "parole",
  "partager", "participe", "particulierement", "partir", "partout", "parvenir",
  "passage", "passerelle", "passion", "patauger", "pates", "patient",
  "patisserie", "patron", "patte", "paupiere", "pauvre", "payer", "paysage",
  "paysan", "peche", "pecheur", "pedale", "pedaler", "peigne", "peindre", "peine",
  "peintre", "peinture", "pelle", "peluche", "pencher", "pendant", "pendre",
  "pendule", "penetrer", "pensee", "penser", "pente", "percer", "perdre", "perdu",
  "periode", "permet", "permettant", "permettent", "permettre", "perroquet",
  "persil", "personnalites", "perte", "peser", "peuple", "peuvent", "pharmacie",
  "pharmacien", "phase", "philippe", "phoque", "photo", "photographier", "phrase",
  "pieds", "pierre", "pigeon", "pilote", "pinceau", "piquer", "piqure", "piscine",
  "pitie", "placard", "place", "placer", "plafond", "plage", "plaindre", "plaine",
  "plaire", "plaisir", "planche", "plante", "planter", "plateau", "platre",
  "plein", "pleurer", "pleuvoir", "plier", "plongeoir", "plonger", "pluie",
  "plume", "plupart", "plusieurs", "plutot", "pluvieux", "poche", "poele",
  "poesie", "poete", "poids", "poignet", "poing", "pointu", "poire", "poireau",
  "poison", "poisson", "poitrine", "police", "policier", "pomme", "pompe",
  "pompier", "poney", "populaire", "population", "portemanteau", "porter",
  "portier", "portiere", "poser", "position", "possede", "posseder", "possible",
  "poste", "poster", "potage", "poubelle", "pouce", "poule", "poulet", "poupee",
  "pourquoi", "pourrait", "poursuivre", "pourtant", "pousser", "poussette",
  "poussiere", "poussin", "poutre", "pouvoir", "prairie", "pratique", "preau",
  "preceder", "precieux", "precipiter", "precis", "preferer", "prend", "prendre",
  "prenom", "preparer", "presence", "presenter", "president", "presque",
  "presser", "pretendre", "preter", "preuve", "prevenir", "prevoir", "prier",
  "priere", "princesse", "principalement", "principaux", "principe", "printemps",
  "prison", "priver", "probablement", "prochain", "proche", "production",
  "produire", "professeur", "profiter", "profondement", "programme", "promenade",
  "promener", "promettre", "prononcer", "proposer", "propre", "protection",
  "proteger", "prouver", "province", "proximite", "prune", "public", "publie",
  "publique", "puisque", "puissance", "puissant", "punir", "puree", "putain",
  "pyjama", "qualite", "quand", "quarante", "quart", "quartier", "quatre",
  "quatrieme", "quebec", "question", "queue", "quinze", "quitte", "quitter",
  "raconter", "radiateur", "radio", "radis", "raisin", "raison", "ramasser",
  "ramener", "ramer", "rampe", "ramper", "rangee", "ranger", "raper", "rapide",
  "rapidement", "rappeler", "rapport", "rapporter", "raquette", "rasoir",
  "rassurer", "rateau", "rater", "rayon", "realisateur", "realise", "realite",
  "recensement", "recette", "recevoir", "recherche", "reciter", "reclamer",
  "recoit", "recommencer", "reconnaitre", "recoudre", "recreation", "recueillir",
  "reculer", "reduire", "reflechir", "reflexion", "refrigerateur", "refuser",
  "regard", "regarder", "regime", "regle", "regne", "regretter", "reine",
  "rejeter", "rejoindre", "rejoint", "relever", "religieux", "religion",
  "remarquer", "remercier", "remettre", "remonter", "remplacer", "remplir",
  "remporte", "remuer", "renard", "rencontre", "rencontrer", "rendre", "renoncer",
  "rentree", "rentrer", "renverser", "repandre", "reparer", "repasser", "repeter",
  "repondre", "reponse", "reposer", "repousser", "reprendre", "representant",
  "represente", "representer", "reprises", "republique", "requin", "reseau",
  "reserver", "resistance", "resister", "resoudre", "respect", "respecter",
  "respirer", "ressembler", "restaurant", "reste", "rester", "retard", "retarder",
  "retenir", "retirer", "retomber", "retour", "retourner", "retrouve",
  "retrouver", "reunir", "reussir", "reveil", "reveiller", "reveler", "revenir",
  "rever", "revoir", "revolution", "revue", "rhinoceros", "riche", "rideau",
  "risquer", "riviere", "robert", "robinet", "roche", "rocher", "roman", "rompre",
  "rondelle", "ronfler", "ronger", "rouge", "roulade", "rouleau", "rouler",
  "route", "royaume", "ruban", "rugueux", "ruine", "russe", "sable", "saigner",
  "saint", "saisir", "saison", "salade", "saladier", "salle", "saluer", "salut",
  "samedi", "sante", "sapin", "sardine", "satisfaire", "sauter", "sauvage",
  "sauver", "savoir", "savon", "scene", "scientifique", "secher", "second",
  "seconde", "secouer", "secours", "secret", "secretaire", "secteur", "section",
  "securite", "seigneur", "selon", "semaine", "semblable", "semble", "sembler",
  "semelle", "sentier", "sentiment", "sentir", "separer", "septembre", "serait",
  "serie", "serieux", "seront", "serpent", "serre", "serrer", "serrure",
  "serviette", "servir", "seuil", "seule", "seulement", "shampoing", "siecle",
  "siege", "sieste", "siffler", "sifflet", "signe", "signer", "signifie",
  "signifier", "silence", "silencieux", "simple", "simplement", "singe",
  "situation", "social", "societe", "soeur", "soigner", "soiree", "soleil",
  "solide", "solitude", "sombre", "somme", "sommeil", "sommet", "songer",
  "sonner", "sonnette", "sorciere", "sorte", "sorti", "sortie", "sortir",
  "soudain", "souffler", "souffrance", "souffrir", "souhaiter", "soulever",
  "souligner", "soumettre", "soupe", "souple", "sourcil", "sourd", "sourire",
  "souris", "soutenir", "souvenir", "souvent", "spectacle", "sportive", "square",
  "squelette", "station", "statistiques", "statut", "structure", "style", "subir",
  "succes", "sucer", "sucre", "sueur", "suffire", "suite", "suivi", "suivre",
  "sujet", "super", "superieur", "supporter", "supposer", "surface", "surprendre",
  "surprise", "surtout", "surveiller", "systeme", "table", "tableau", "tablier",
  "tabouret", "tache", "taille", "tailler", "talon", "tambour", "tandis",
  "tantot", "taper", "tapis", "tarte", "tartine", "tasse", "tater", "taupe",
  "telecommande", "telephone", "telephoner", "television", "tellement", "temoin",
  "tempete", "temps", "tendre", "tenir", "tenter", "terme", "termine", "terminer",
  "terrain", "terreur", "terrible", "terrier", "territoire", "teter", "theatre",
  "theorie", "thermometre", "ticket", "tigre", "timbre", "tirer", "tiroir",
  "tissu", "toboggan", "toile", "toilette", "tomate", "tombe", "tomber",
  "tonnerre", "torchon", "tordu", "tortue", "total", "toucher", "toujours",
  "tournee", "tourner", "tournevis", "tournoi", "tousser", "toutefois", "trace",
  "tracer", "tracteur", "train", "traineau", "trainer", "traire", "trait",
  "traiter", "trampoline", "tranche", "tranquille", "transformer", "transparent",
  "transpirer", "transport", "transporter", "travail", "travaille", "travailler",
  "travaux", "travers", "traverser", "trembler", "tremper", "trente", "tresor",
  "tricher", "tricot", "tricoter", "tricycle", "trier", "triste", "trois",
  "troisieme", "tromper", "trompette", "trottoir", "troubler", "trouer",
  "troupes", "trousse", "trouve", "trouver", "tulipe", "tunnel", "tuyau",
  "uniforme", "unique", "univers", "university", "usage", "utile", "utiliser",
  "vacances", "vache", "vague", "vaincre", "vainqueur", "vaisselle", "valeur",
  "valise", "vallee", "valoir", "vaste", "vehicule", "veille", "veiller",
  "vendre", "vendredi", "venir", "venter", "ventre", "veritable", "verite",
  "verre", "verser", "version", "veste", "vetement", "veterinaire", "vetir",
  "viande", "victime", "victoire", "video", "vider", "vieil", "vieillard",
  "vient", "vieux", "village", "vingt", "violence", "violent", "virage", "visage",
  "visible", "vision", "visite", "visiter", "vitesse", "vitre", "vivant", "vivre",
  "voici", "voila", "voile", "voisin", "voiture", "voler", "volet", "volonte",
  "vote", "votre", "vouloir", "voyage", "voyager", "vraiment", "wagon",
  "xylophone", "yaourt", "zebre", "zigzag", "zones"
  ]
