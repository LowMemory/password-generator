require 'securerandom'

require 'bundler'
Bundler.require

Sinatra::Sprockets.configure do |config|
  config.compile = ['passphrase.js']         # On-the-fly compilation
  config.compress = true        # Compress assets
  config.digest = true          # Append a digest to URLs
  config.css_compressor = YUI::CssCompressor.new  # CSS compressor instance
  config.js_compressor = Uglifier.new   # JS compressor instance
end

class App < Sinatra::Base

  register Sinatra::Sprockets

  set :markdown, :layout_engine => :haml

  get '/' do
    @seed = SecureRandom.hex
    haml :index
  end

end
