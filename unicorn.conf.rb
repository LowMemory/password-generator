worker_processes 4
listen "unix:tmp/unicorn.sock", :backlog => 64
listen "127.0.0.1:9090", :tcp_nopush => true
pid "./tmp/unicorn.pid"
